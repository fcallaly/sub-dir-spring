package com.conygre.training.trader.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.junit.jupiter.api.Test;

public class TradeTests {
    
    @Test
    public void test_Trade_gettersSetters(){
        Trade testTrade = new Trade();
        Date testDate = new Date();
        TradeType testType = TradeType.BUY;
        TradeState testState = TradeState.PROCESSING;
        String testTicker = "AMZN";
        double testPrice = 99.99;
        int testQuantity = 245;

        testTrade.setCreated(testDate);
        testTrade.setPrice(testPrice);
        testTrade.setQuantity(testQuantity);
        testTrade.setState(testState);
        testTrade.setTicker(testTicker);
        testTrade.setType(testType);

        assertEquals(testTrade.getCreated(), testDate);
        assertEquals(testTrade.getPrice(), testPrice);
        assertEquals(testTrade.getQuantity(), testQuantity);
        assertEquals(testTrade.getState(), testState);
        assertEquals(testTrade.getTicker(), testTicker);
        assertEquals(testTrade.getType(), testType);
    }
}
